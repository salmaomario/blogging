<?php

namespace App\Http\Controllers;

//use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Request;
use DB;

class ArticlesController extends Controller {
    
    public function index() {  
        $data['articles'] = $articles = DB::table('articles')->get();
        return view('articles/index', compact('data'));
    }
    public function view($article_id) {
        $data['articles'] = DB::table('articles')
                ->where(array('articles.id' => $article_id))
                ->join('categories', 'articles.category_id', '=', 'categories.id')
                ->first();
        $data['comments'] = DB::table('articles_comments')
                ->where(array('articles_comments.article_id' => $article_id))
                ->get();
        $data['article_id'] = $article_id;
        return view('articles/view', compact('data'));
    }
    public function add(){
        if(Request::isMethod('post')) {
            $all = Request::all();
            $article_id = DB::table('articles')->insertGetId(array(
                'title' => $all['title'],
                'body' => $all['body'],
                'category_id' => $all['category']
            ));            
            return redirect('articles/view/'.$article_id);
        } else {
            $data['categories'] = DB::table('categories')->get();
            return view('articles/add', compact('data'));
        }
        
    }
    public function category($category_id) {
        $data['articles'] = DB::table('articles')->where(array('category_id' => $category_id))->get();
        $data['category'] = DB::table('categories')->where(array('id' => $category_id))->pluck('name');;
        return view('articles/category', compact('data'));
    }
    public function addComment() {
        $all = Request::all();
        DB::table('articles_comments')->insert(array(
            'article_id' => $all['article_id'],
            'comment_title' => $all['comment_title'],
            'comment_text' => $all['comment_text'],
            'commenter' => $all['commenter']
        ));
        return redirect('articles/view/'.$all['article_id']);
     }
}
