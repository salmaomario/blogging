@extends('default')
 
@section('content')
<style>
    .blue { color: blue }
    .red { color: red}
</style>
<h3  class="post-title entry-title blue">{{$data['articles']->title}}</h3>
<div class="post-body entry-content" id="post-body-1801283148241728182">
    {{$data['articles']->body}}
    <br>
    <div style="clear: both;"></div>
</div>
<hr>
<div class="comments" id="comments">
    <h4 class="red">Comments</h4>
    <hr>
    @foreach($data['comments'] as $comment)
    <div id="Blog1_comments-block-wrapper">
        <h4 class="blue">{{$comment->comment_title}}</h4>
        <p>{{$comment->comment_text}} </p>
        <b>{{$comment->commenter}}</b>
    </div>
    <hr>
    @endforeach
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Add New Comment</div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{URL::to('/')}}/articles/addComment">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="article_id" value="{{ $data['article_id'] }}">
                        
                        <div class="form-group">
                            <label class="col-md-4 control-label">Your Name</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="commenter" value="{{ old('commenter') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">Title</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="comment_title">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Text</label>
                            <div class="col-md-6">
                                <textarea class="form-control" rows="4" cols="7" name="comment_text"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Add Comment
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection