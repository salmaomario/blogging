@extends('default')
 
@section('content')
<h1>All Articles</h1>
@foreach($data['articles'] as $article)
<a href='{{URL::to('/')}}/articles/view/{{$article->id}}'>
    <h3 class="post-title entry-title">
        {{$article->title}}
    </h3>
</a>
<div class="post-body entry-content" id="post-body-1801283148241728182">
    {{$article->body}}
    <br>
    <div style="clear: both;"></div>
</div>
<hr>
@endforeach
@endsection